Categories:Games
License:GPLv3
Web Site:http://www.dozingcatsoftware.com
Source Code:https://github.com/dozingcat/Vector-Pinball
Issue Tracker:https://github.com/dozingcat/Vector-Pinball/issues

Auto Name:Vector Pinball
Summary:Pinball game
Description:
Vector Pinball is a pinball game.
.

Repo Type:git
Repo:https://github.com/dozingcat/Vector-Pinball.git

Build:1.1,4
    commit=45b5218594320ffb4b37

Build:1.3,10
    commit=1210949b1e373916d096

Build:1.3.1,11
    commit=04ee044b27
    target=android-10

Build:1.4.2,14
    commit=5bb3c7d64ef3bcc8e8f7d97f55353d6b6a7b68cd
    srclibs=libgdx@1.4.1
    rm=libs/*
    disable=todo libgdx

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.4.2
Current Version Code:14

